var basis = require('alpi_basis');

module.exports = {
    /**
     * @summary List all possible protocols.
     * @description More informations at: http://www.linux-france.org/article/man-fr/man5/protocols-5.html .
     * @param {Function} callback 
     */
    List: function (callback) {
        basis.System.ShellCommand("cat /etc/protocols | grep -v '#' | tr -s ' '", (e) => {
            if (e.stderr === "") {
                var informations = e.stdout.trim().split('\n');
                var protocols = {};
                informations.forEach(line => {
                    line = line.split(' ');
                    protocols[line[0]] = {
                        protocol: line[0],
                        number: line[1],
                        alias: line[2]
                    };
                });
                callback(protocols);
            } else {
                callback(false);
            }
        });
    },
    /**
     * @summary Check if the parameter protocolName match one of the posssible protocols.
     * @param {String} protocolName 
     * @param {Function} callback
     */
    Exists: function (protocolName, callback) {
        module.exports.List((protocols) => {
            callback(Object.keys(protocols).includes(protocolName));
        });
    }
};