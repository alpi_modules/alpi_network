var basis = require('alpi_basis');
var filters = require('alpi_filters');
var devices = require('./devices');

module.exports = {
    /**
     * List connection profiles.
     * @param {Function} callback - Object that contains all connections.
     */
    List: function (callback) {
        basis.System.ShellCommand("nmcli -t connection", (connections) => {
            if (connections.stderr === "") {
                var tmp = connections.stdout.trim().split("\n");
                connections = {};
                tmp.forEach((connection) => {
                    connection = connection.split(":");
                    connections[connection[0]] = {
                        "name": connection[0],
                        "uuid": connection[1],
                        "type": connection[2],
                        "device": connection[3]
                    };
                });
                callback(connections);
            } else {
                callback(false);
            }
        });
    },
    /**
     * List active connection profiles.
     * @param {Function} callback - Object that contains all actives connections.
     */
    ListActive: function (callback) {
        basis.System.ShellCommand("nmcli -t connection show --active", (connections) => {
            if (connections.stderr === "") {
                var tmp = connections.stdout.trim().split("\n");
                connections = {};
                tmp.forEach((connection) => {
                    connection = connection.split(":");
                    connections[connection[0]] = {
                        "name": connection[0],
                        "uuid": connection[1],
                        "type": connection[2],
                        "device": connection[3]
                    };
                });
                callback(connections);
            } else {
                callback(false);
            }
        });
    },
    /**
     * List details for a given connection profile.
     * @param {String} connectionUUID - Connection profile uuid or name.
     * @param {Function} callback - False/Object that contains all the options of the specified connection.
     */
    GetNetworkConfiguration(connectionUUID, callback) {
        connectionUUID = filters.KeepOnly.HexadecimalAndHyphens(connectionUUID);
        basis.System.ShellCommand("nmcli -t connection show " + connectionUUID, (informations) => {
            if (informations.stderr === "") {
                var tmp = informations.stdout.trim().split("\n");
                informations = {};
                tmp.forEach((information) => {
                    information = information.split(":");
                    information[0] = information[0].split(".");
                    if (!informations.hasOwnProperty(information[0][0])) {
                        informations[information[0][0]] = {};
                    }
                    informations[information[0][0]][information[0][1]] = information[1];
                });
                callback(informations);
            } else {
                callback(false);
            }
        });
    },
    /**
     * Lists connection types with their options.
     * @return {Object} - Object that contains network connection types with theis specific options.
     */
    GetNetworkTypesWithSpecificOptions() {
        return {
            "ethernet": {
                "optional": {
                    "mac": "<MAC address>",
                    "cloned-mac": "<cloned MAC address>",
                    "mtu": "<MTU>"
                }
            },
            "wifi": {
                "needed": {
                    "ssid": "<SSID>"
                },
                "optional": {
                    "mac": "<MAC address>",
                    "cloned-mac": "<cloned MAC address>",
                    "mtu": "<MTU>",
                    "mode": ["infrastructure", "ap", "adhoc"]
                }
            },
            "wimax": {
                "optional": {
                    "mac": "<MAC address>",
                    "nsp": "<NSP>"
                }
            },
            "pppoe": {
                "needed": {
                    "username": "<PPPoE username>",
                },
                "optional": {
                    "password": "<PPPoE password>",
                    "service": "<PPPoE service name>",
                    "mtu": "<MTU>",
                    "mac": "<MAC address>"
                }
            },
            "gsm": {
                "needed": {
                    "apn": "<APN>"
                },
                "optional": {
                    "user": "<username>",
                    "password": "<password>"
                }
            },
            "cdma": {
                "optional": {
                    "user": "<username>",
                    "password": "<password>"
                }
            },
            "infiniband": {
                "optional": {
                    "mac": "<MAC address>",
                    "mtu": "<MTU>",
                    "transport-mode": ["datagram", "connected"],
                    "parent": "<ifname>",
                    "p-key": "<IPoIB P_Key>"
                }
            },
            "bluetooth": {
                "optional": {
                    "addr": "<bluetooth address>",
                    "bt-type": ["panu", "nap", "dun-gsm", "dun-cdma"]
                }
            },
            "vlan": {
                "needed": {
                    "dev": "<parent device (connection UUID, ifname, or MAC)>",
                    "id": "<VLAN ID>",
                },
                "optional": {
                    "flags": "<VLAN flags>",
                    "ingress": "<ingress priprity mapping>",
                    "egress": "<egress priority mapping>",
                    "mtu": "<MTU>"
                }
            },
            "bond": {
                "optional": {
                    "mode": ["balance-rr", "active-backup", "balance-xor", "broadcast", "802.3ad", "balance-tlb", "balance-alb"],
                    "primary": "<ifname>",
                    "miimon": "<num>",
                    "downdelay": "<num>",
                    "updelay": "<num>",
                    "arp-interval": "<num>",
                    "arp-ip-target": "<num>",
                    "lacp-rate": ["slow", "fast"]
                }
            },
            "bond-slave": {
                "needed": {
                    "master": "<master (ifname, or connection UUID, or name)>"
                }
            },
            "team": {
                "optional": {
                    "config": ["<file>", "<raw JSON data>"]
                }
            },
            "team-slave": {
                "needed": {
                    "master": "<master (ifname, or connection UUID or name)>",
                },
                "optional": {
                    "config": ["<file>", "<raw JSON data>"]
                }
            },
            "bridge": {
                "optional": {
                    "stp": ["yes", "no"],
                    "priority": "<num>",
                    "forward-delay": "<2-30>",
                    "hello-time": "<1-10>",
                    "max-age": "<6-40>",
                    "ageing-time": "<0-1000000>",
                    "multicast-snooping": ["yes", "no"],
                    "mac": "<MAC address>"
                }
            },
            "bridge-slave": {
                "needed": {
                    "master": "<master (ifname, or connection UUID or name)>"
                },
                "optional": {
                    "priority": "<0-63>",
                    "path-cost": "<1-65535>",
                    "hairpin": ["yes", "no"]
                }
            },
            "vpn": {
                "needed": {
                    "vpn-type": ["vpnc", "openvpn", "pptp", "openconnect", "openswan", "libreswan", "ssh", "l2tp", "iodine", "..."]
                },
                "optional": {
                    "user": "<username>"
                }
            },
            "olpc-mesh": {
                "needed": {
                    "ssid": "<SSID>",
                },
                "optional": {
                    "channel": "<1-13>",
                    "dhcp-anycast": "<MAC address>"
                }
            },
            "adsl": {
                "needed": {
                    "username": "<username>",
                    "protocol": ["pppoa", "pppoe", "ipoatm"]
                },
                "optional": {
                    "password": "<password>",
                    "encapsulation": ["vcmux", "llc"]
                }
            },
            "tun": {
                "needed": {
                    "mode": ["tun", "tap"],
                },
                "optional": {
                    "owner": "<UID>",
                    "group": "<GID>",
                    "pi": ["yes", "no"],
                    "vnet-hdr": ["yes", "no"],
                    "multi-queue": ["yes", "no"]
                }
            },
            "ip-tunnel": {
                "needed": {
                    "mode": ["ipip", "gre", "sit", "isatap", "vti", "ip6ip6", "ip6gre", "vti6"],
                    "remote": "<remote endpoint IP>"
                },
                "optional": {
                    "local": "<local endpoint IP>",
                    "dev": "<parent device (ifname or connection UUID)>"
                }
            },
            "macsec": {
                "needed": {
                    "dev": "<parent device (connection UUID, ifname, or MAC)>",
                    "mode": ["psk", "eap"]
                },
                "optional": {
                    "cak": "<key>",
                    "cnk": "<key>",
                    "encrypt": ["yes", "no"],
                    "port": "1-65534"
                }
            },
            "macvlan": {
                "needed": {
                    "dev": "<parent device (connection UUID, ifname, or MAC>",
                    "mode": ["vepa", "bridge", "private", "passthru", "source"],
                },
                "optional": {
                    "tap": ["yes", "no"]
                }
            },
            "vxlan": {
                "needed": {
                    "id": "<VXLAN ID>",
                    "remote": "<IP of multicast group or remote address>"
                },
                "optional": {
                    "local": "<source IP>",
                    "dev": "<parent device (ifname or connection UUID)>",
                    "source-port-min": "<0-65535>",
                    "source-port-max": "<0-65535>",
                    "destination-port": "<0-65535>"
                }
            }
        };
    },
    /**
     * Lists options for a specific connection type.
     * @param {String} type - Connection type.
     * @return {Object} - Object that contains type specific options.
     */
    GetTypeSpecificOptions(type) {
        return module.exports.GetNetworkTypesWithSpecificOptions().hasOwnProperty(type) === true ? module.exports.GetNetworkTypesWithSpecificOptions()[type] : false;
    },
    /**
     * Lists ip options.
     * @return {Object} - Object that contains ip options.
     */
    GetIpOptions: function () {
        return {
            "optional": {
                "ip4": "<IPv4 address>",
                "gw4": "<IPv4 gateway>",
                "ip6": "<IPv6 address>",
                "gw6": "<IPv6 gateway>"
            }
        };
    },
    /**
     * Create a new connection using specified properties.
     * @param {String} type - type of connection
     * @param {String} interfaceName - interface name 
     * @param {String} connectionName - nmcli conneciton name
     * @param {Boolean} autoconnect - Control whether the connection should autoconnect.
     * @param {Boolean} save - Controls whether the connection should be persistent,
     * @param {Object} typeSpecificOptions - Object that contains options specific to the chosen type.
     * @param {Object} ipOptions - Object that contains ip options.
     * @param {Function} callback - Bool/String False or the new connection UUID.
     */
    AddNetworkConfiguration(type, interfaceName, connectionName, autoconnect, save, typeSpecificOptions, ipOptions, callback) {
        var add = true;
        var allTypeSpecificOptions = module.exports.GetTypeSpecificOptions(type);
        if (module.exports.GetNetworkTypesWithSpecificOptions().hasOwnProperty(type)) {
            devices.Exists(interfaceName, (exists) => {
                if (exists === true) {
                    if (JSON.stringify(Object.keys(typeSpecificOptions)) === JSON.stringify(Object.keys(allTypeSpecificOptions))) {
                        // @ts-ignore
                        autoconnect = (autoconnect === true) ? "yes" : "no";
                        // @ts-ignore
                        save = (save === true) ? "yes" : "no";
                        var command = "nmcli connection add type " + type + " ifname " + interfaceName + " con-name " + connectionName + " autoconnect " + autoconnect + " save " + save;
                        //ajout des options spécifiques au type à la commande
                        Object.keys(typeSpecificOptions).forEach((optionType) => {
                            if (JSON.stringify(Object.keys(typeSpecificOptions[optionType])) === JSON.stringify(Object.keys(allTypeSpecificOptions[optionType]))) {
                                Object.keys(typeSpecificOptions[optionType]).forEach((option) => {
                                    if (typeof typeSpecificOptions[optionType][option] === "string" && typeSpecificOptions[optionType][option] !== allTypeSpecificOptions[optionType][option]) {
                                        command += " " + option + " '" + typeSpecificOptions[optionType][option] + "'";
                                    }
                                });
                            } else {
                                add = false;
                            }
                        });
                        var allIpOptions = module.exports.GetIpOptions();
                        Object.keys(ipOptions).forEach((optionType) => {
                            if (JSON.stringify(Object.keys(ipOptions[optionType])) === JSON.stringify(Object.keys(allIpOptions[optionType]))) {
                                Object.keys(ipOptions[optionType]).forEach((option) => {
                                    if (typeof ipOptions[optionType][option] === "string" && ipOptions[optionType][option] !== allIpOptions[optionType][option]) {
                                        command += " " + option + " '" + ipOptions[optionType][option] + "'";
                                    }
                                });
                            } else {
                                add = false;
                            }
                        });
                        if (add === true) {
                            basis.System.ShellCommand(command, (e) => {
                                if (e.stderr === "") {
                                    var output = e.stdout.split(" ");
                                    callback((output[output.length - 3]).slice(1, -1));
                                } else {
                                    callback(false);
                                }
                            });
                        } else {
                            callback(false);
                        }
                    } else {
                        //les options spécifiées ne sont pas conformes
                        callback(false);
                    }
                } else {
                    callback(false);
                }
            });
        } else {
            //wrong type
            callback(false);
        }
    },
    /**
     * Modify a connection profile. 
     * @param {String} connectionUUID - Connection profile uuid or name.
     * @param {Object} settings - Object that contains all settings related to the connection with modifications
     * @param {Function} callback - Boolean
     */
    ModifyNetworkConfiguration(connectionUUID, settings, callback) {
        connectionUUID = filters.KeepOnly.HexadecimalAndHyphens(connectionUUID);
        module.exports.GetNetworkConfiguration(connectionUUID, (currentSettings) => {
            if (currentSettings !== false) {
                var execute = false;
                if (JSON.stringify(Object.keys(settings)) === JSON.stringify(Object.keys(currentSettings))) {
                    var command = "nmcli connection modify " + connectionUUID;
                    Object.keys(settings).forEach((setting) => {
                        if (typeof settings[setting] === "object") {
                            Object.keys(settings[setting]).forEach((s) => {
                                if (typeof settings[setting][s] === "string") {
                                    if (settings[setting][s] !== currentSettings[setting][s]) {
                                        execute = true;
                                        command += " " + setting + "." + s + " '" + settings[setting][s] + "'";
                                    }
                                } else {
                                    callback(false);
                                }
                            });
                        } else {
                            callback(false);
                        }
                    });
                    // @ts-ignore
                    if (execute === true) {
                        basis.System.ShellCommand(command, (e) => {
                            callback(e.stderr === "");
                        });
                    } else {
                        callback(true);
                    }
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * Delete a configured connection.
     * @param {String} connectionUUID - Connection profile uuid or name.
     * @param {Function} callback - Boolean.
     */
    DeleteNetworkConfiguration(connectionUUID, callback) {
        connectionUUID = filters.KeepOnly.HexadecimalAndHyphens(connectionUUID);
        basis.System.ShellCommand("nmcli connection delete " + connectionUUID, (e) => {
            callback(e.stderr === "");
        });
    },
    /**
     * Sets the wifi password for a ssid.
     * @param {String} ssid - essid or bssid of the access point.
     * @param {String} password - password of the access point.
     * @param {Function} callback - Boolean.
     */
    SetWirelessNetworkPassword(ssid, password, callback) {
        basis.System.ShellCommand("sudo nmcli device wifi connect " + ssid + " password " + password, (e) => {
            callback(e.stderr === "");
        });
    },
    /**
     * Up a network configuration.
     * @param {String} connectionUUID - Connection profile uuid or name.
     * @param {Function} callback - Boolean.
     */
    UpNetworkConfiguration(connectionUUID, callback) {
        connectionUUID = filters.KeepOnly.HexadecimalAndHyphens(connectionUUID);
        basis.System.ShellCommand("nmcli connection up " + connectionUUID, (e) => {
            callback(e.stderr === "");
        });
    },
    /**
     * Down a network configuration.
     * @param {String} connectionUUID - Connection profile uuid or name.
     * @param {Function} callback - Boolean.
     */
    DownNetworkConfiguration(connectionUUID, callback) {
        connectionUUID = filters.KeepOnly.HexadecimalAndHyphens(connectionUUID);
        basis.System.ShellCommand("nmcli connection down " + connectionUUID, (e) => {
            callback(e.stderr === "");
        });
    },
};