var basis = require('alpi_basis');

module.exports = {
    /**
     * Get the status of NetworkManager's networking.
     * 
     * @param {Function} callback - false/status of the networking.
     */
    Status: function (callback) {
        basis.System.ShellCommand("nmcli networking", (status) => {
            callback(status.stderr === "" ? status.stdout : false);
        });
    },
    /**
     * Enables the NetworkManager's networking.
     * @param {Function} callback - Boolean.
     */
    Enable: function (callback) {
        basis.System.ShellCommand("nmcli networking on", (e) => {
            callback(e.stderr === "");
        });
    },
    /**
     * Disables the NetworkManager's networking.
     * @param {Function} callback - Boolean.
     */
    Disable: function (callback) {
        basis.System.ShellCommand("nmcli networking off", (e) => {
            callback(e.stderr === "");
        });
    },
    /**
     * Gets the NetworkManager's connectivity.
     * @param {Function} callback - false/status of the networking connectivity.
     */
    GetConnectivity: function (callback) {
        basis.System.ShellCommand("nmcli networking connectivity", (e) => {
            callback(e.stderr === "" ? e.stdout : false);
        });
    }
};