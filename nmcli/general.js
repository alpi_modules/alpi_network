var basis = require('alpi_basis');
var filters = require('alpi_filters');

module.exports = {
    /**
     * Gets General inforamtions from NetworkManager
     * 
     * @param {Function} callback - 
     */
    GetGeneralInformations: function (callback) {
        basis.System.ShellCommand("nmcli -t general", (informations) => {
            if (informations.stderr === "") {
                informations = informations.stdout.split(":");
                callback({
                    "state": informations[0],
                    "connectivity": informations[1],
                    "wifi-hw": informations[2],
                    "wifi": informations[3],
                    "wwan-hw": informations[4],
                    "wwan": informations[5]
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * Get persistent system hostname.
     * @param {Function} callback - false/String that contains the system hostname.
     */
    GetHostname: function (callback) {
        basis.System.ShellCommand("nmcli general hostname", (hostname) => {
            if (hostname.stderr === "") {
                callback(hostname.stdout);
            } else {
                callback(false);
            }
        });
    },
    /**
     * Sets the hostname.
     * @param {Function} callback - Boolean.
     */
    SetHostname: function (hostname, callback) {
        hostname = filters.KeepOnly.Services(hostname);
        basis.System.ShellCommand("nmcli general hostname " + hostname, (e) => {
            callback(e.stderr === "");
        });
    },
    /**
     * Get the current logging level and domains of NetworkManager.
     * @param {Function} callback - false/Object that contains differents loggings levels.
     */
    GetCurrentLoggingLevel: function (callback) {
        basis.System.ShellCommand("nmcli -t general logging", (levels) => {
            if (levels.stderr === "") {
                var tmp = levels.split("\n");
                levels = {};
                tmp.forEach((line) => {
                    line = line.split(":");
                    levels[line[0]] = line[1].split(",");
                });
                callback(levels);
            } else {
                callback(false);
            }
        });
    },
    /**
     * Retrieves the possible logging levels of NetworkManager.
     * @param {Function} callback - Get differents possibles loggings levels.
     */
    GetLoggingLevels: function (callback) {
        callback(["ERR", "WARN", "INFO", "DEBUG"]);
    },
    /**
     * Retrieves the possible logging domains of NetworkManager.
     * @param {Function} callback - Get differents possibles loggings domains.
     */
    GetLoggingDomains: function (callback) {
        callback(["PLATFORM", "RFKILL", "ETHER", "WIFI", "BT", "MB", "DHCP4", "DHCP6", "PPP", "WIFI_SCAN", "IP4", "IP6", "AUTOIP4", "DNS", "VPN", "SHARING", "SUPPLICANT", "AGENTS", "SETTINGS", "SUSPEND", "CORE", "DEVICE", "OLPC", "WIMAX", "INFINIBAND", "FIREWALL", "ADSL", "BOND", "VLAN", "BRIDGE", "DBUS_PROPS", "TEAM", "CONCHECK", "DCB", "DISPATCH", "AUDIT", "SYSTEMD", "VPN_PLUGIN", "PROXY"]);
    },
    /**
     * Sets the logging level and domains.
     * @param {String} level - A debugging level.
     * @param {String[]} domains - List of domains.
     * @param {Function} callback - Boolean
     */
    SetLoggingLevel: function (level, domains, callback) {
        if (level.match(/^(ERR|WARN|INFO|DEBUG)$/) !== null) {
            var domainsValid = true;
            domains.forEach((domain) => {
                if (domain.match(/^(PLATFORM|RFKILL|ETHER|WIFI|BT|MB|DHCP4|DHCP6|PPP|WIFI_SCAN|IP4|IP6|AUTOIP4|DNS|VPN|SHARING|SUPPLICANT|AGENTS|SETTINGS|SUSPEND|CORE|DEVICE|OLPC|WIMAX|INFINIBAND|FIREWALL|ADSL|BOND|VLAN|BRIDGE|DBUS_PROPS|TEAM|CONCHECK|DCB|DISPATCH|AUDIT|SYSTEMD|VPN_PLUGIN|PROXY)$/) === null) {
                    domainsValid = false;
                }
            });
            if (domainsValid === true) {
                // @ts-ignore
                domains = domains.join(",");
                basis.System.ShellCommand("sudo nmcli general logging level " + level + " domain " + domains, (e) => {
                    callback(e.stderr === "");
                });
            } else {
                callback(false);
            }
        } else {
            callback(false);
        }
    },
    /**
     * Gets the permissions of NetworkManager.
     * @param {Function} callback - false/Object that contains list of permissions.
     */
    GetPermissions: function (callback) {
        basis.System.ShellCommand("nmcli -t general permissions", (permissions) => {
            // callback(permissions.stderr === "" ? permissions.stdout : false);
            if (permissions.stderr === "") {
                var tmp = permissions.stdout.split("\n");
                permissions = {};
                tmp.forEach((line) => {
                    line = line.split(":");
                    permissions[line[0]] = line[1];
                });
                callback(permissions);
            } else {
                callback(false);
            }
        });
    }
};