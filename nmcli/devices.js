var basis = require('alpi_basis');

module.exports = {
    /**
     * List details of devices.
     * @param {Function} callback - false/Object that contains a list of devices with their informations.
     */
    List: function (callback) {
        basis.System.ShellCommand("nmcli -t device", (interfaces) => {
            if (interfaces.stderr === "") {
                var tmp = interfaces.stdout.split("\n");
                interfaces = {};
                tmp.forEach((iface) => {
                    iface = iface.split(":");
                    interfaces[iface[0]] = {
                        "DEVICE": iface[0],
                        "TYPE": iface[1],
                        "STATE": iface[2],
                        "CONNECTION": iface[3]
                    };
                });
                callback(interfaces);
            } else {
                callback(false);
            }
        });
    },
    /**
     * Checks if a device exists.
     * @param {String} interfaceName - Interface name.
     * @param {Function} callback - Boolean.
     */
    Exists: function (interfaceName, callback) {
        module.exports.List(interfaces => {
            callback(interfaces.hasOwnProperty(interfaceName));
        });
    },
    /**
     * List details of a specified device.
     * @param {String} interfaceName - Interface name.
     * @param {Function} callback - false/Object that contains the informations of a specified device.
     */
    InterfaceInformations: function (interfaceName, callback) {
        module.exports.Exists(interfaceName, (e) => {
            if (e === true) {
                basis.System.ShellCommand("nmcli -t device show " + interfaceName, (informations) => {
                    if (informations.stderr === "") {
                        var tmp = informations.stdout.trim().split("\n");
                        informations = {};
                        tmp.forEach((information) => {
                            information = [information.substr(0, information.indexOf(':')), information.substr(information.indexOf(":") + 1)];
                            information[0] = information[0].split(".");
                            if (informations.hasOwnProperty(information[0][0]) === false) {
                                informations[information[0][0]] = {};
                            }
                            informations[information[0][0]][information[0][1]] = information[1];
                        });
                        callback(informations);
                    } else {
                        callback(false);
                    }
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * @summary Connect the device.
     * NetworkManager will try to find a suitable connection that will be activated.
     * It will also consider connections that are not set to auto-connect.
     * @param {String} interfaceName - Interface name.
     * @param {Function} callback - Boolean.
     */
    Connect: function (interfaceName, callback) {
        basis.System.ShellCommand("nmcli device connect " + interfaceName, (e) => {
            callback(e.stderr === "");
        });
    },
    /**
     * @summary Disconnect the device.
     * The command disconnects the device and prevents it from auto-activating further connections without user/manual intervention.
     * @param {String} interfaceName - Interface name.
     * @param {Function} callback - Boolean.
     */
    Disconnect: function (interfaceName, callback) {
        basis.System.ShellCommand("nmcli device disconnect " + interfaceName, (e) => {
            callback(e.stderr === "");
        });
    },
    /**
     * @summary Delete the software device.
     * It only works for software devices (like bonds, bridges, etc.). Hardware devices cannot be deleted by the command.
     * @param {String} interfaceName - Interface name
     * @param {Function} callback - Boolean.
     */
    Delete: function (interfaceName, callback) {
        basis.System.ShellCommand("nmcli device delete " + interfaceName, (e) => {
            callback(e.stderr === "");
        });
    },
    /**
     * List neighboring devices discovered through LLDP.
     * @param {Function} callback - Boolean.
     */
    Lldp: function (callback) {
        basis.System.ShellCommand("nmcli device lldp list", (e) => {
            callback(e.stderr === "");
        });
    },
    /**
     * List neighboring devices discovered through LLDP for a given interface.
     * @param {String} interfaceName - Interface name.
     * @param {Function} callback - Boolean.
     */
    LldpInterface: function (interfaceName, callback) {
        basis.System.ShellCommand("nmcli device lldp list ifname " + interfaceName, (e) => {
            callback(e.stderr === "");
        });
    },
    /**
     * Modify one or more properties currently active on the device without modifying the connection profile.
     * @param {String} interfaceName - Interface name.
     * @param {Object} interfaceProperties - Object that contains all properties related to the interface (function InterfaceInformations()).
     * @param {Function} callback - Boolean.
     */
    Modify: function (interfaceName, interfaceProperties, callback) {
        module.exports.Exists(interfaceName, (exists) => {
            if (exists === true) {
                var execute = false;
                module.exports.InterfaceInformations(interfaceName, (currentInterfaceProperties) => {
                    if (currentInterfaceProperties !== false) {
                        if (JSON.stringify(Object.keys(interfaceProperties)) === JSON.stringify(Object.keys(currentInterfaceProperties))) {
                            var command = "nmcli device modify " + interfaceName;
                            Object.keys(interfaceProperties).forEach((property) => {
                                if (typeof interfaceProperties[property] === "object") {
                                    interfaceProperties[property].forEach((p) => {
                                        if (interfaceProperties[property][p] !== currentInterfaceProperties[property][p]) {
                                            execute = true;
                                            command += " " + property + "." + p + " '" + interfaceProperties[property][p] + "'";
                                        }
                                    });
                                } else {
                                    callback(false);
                                }
                            });
                        } else {
                            callback(false);
                        }
                    } else {
                        callback(false);
                    }
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * Attempts to update device with changes to the currently active connection made since it was last applied.
     * @param {String} interfaceName - Interface name.
     * @param {Function} callback Boolean.
     */
    Reapply: function (interfaceName, callback) {
        module.exports.Exists(interfaceName, (exists) => {
            if (exists === true) {
                basis.System.ShellCommand("nmcli device reapply " + interfaceName, (e) => {
                    callback(e.stderr === "");
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * Modify device properties.
     * @param {String} interfaceName - Interface name.
     * @param {Boolean} autoconnect - Control whether the device should autoconnect.
     * @param {Boolean} managed - Control whether the device is managed or not.
     * @param {Function} callback - Boolean.
     */
    Set: function (interfaceName, autoconnect, managed, callback) {
        module.exports.Exists(interfaceName, (exists) => {
            if (exists === true) {
                // @ts-ignore
                autoconnect = autoconnect === true ? "yes" : "no";
                // @ts-ignore
                managed = managed === true ? "yes" : "no";
                basis.System.ShellCommand("nmcli device set " + interfaceName + " autoconnect " + autoconnect + " managed " + managed, (e) => {
                    callback(e.stderr === "");
                });
            } else {
                callback(false);
            }
        });
    },
    Wifi: require('./device/wifi')
};