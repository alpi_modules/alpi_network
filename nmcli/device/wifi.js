// @ts-ignore
var basis = require('alpi_basis');

module.exports = {
    /**
     * List available Wi-Fi access points.
     * 
     * @param {Function} callback false/Object that contains informations about available wi-fi access points.
     */
    List: function (callback) {
        basis.System.ShellCommand("nmcli -t -f in-use,ssid,bssid,mode,chan,rate,signal,bars,security device wifi", (accessPoints) => {
            if (accessPoints.stderr !== false) {
                var tmp = accessPoints.stdout.trim().split("\n");
                accessPoints = {};
                tmp.forEach((ap) => {
                    ap = ap.replace(/\\:/g, ".");
                    ap = ap.split(":");
                    accessPoints[ap[1]] = {
                        'IN-USE': ap[0].trim() !== "",
                        "SSID": ap[1],
                        "BSSID": ap[2].replace(/\./g, ":"),
                        "MODE": ap[3],
                        "CHAN": ap[4],
                        "RATE": ap[5],
                        "SIGNAL": ap[6],
                        "BARS": ap[7],
                        "SECURITY": ap[8]
                    };
                });
                callback(accessPoints);
            } else {
                callback(false);
            }
        });
    },
    /**
     * Request that NetworkManager immediately re-scan for available access points.
     * @param {Function} callback - Boolean
     */
    Rescan: function (callback) {
        basis.System.ShellCommand("nmcli device wifi rescan", (e) => {
            callback(e.stderr == true);
        });
    },
    /**
     * Connect to a Wi-Fi network specified by SSID or BSSID.
     * @param {String} interfaceName
     * @param {Boolean|String} connectionName - Name of the connection or false for nmcli auto naming.
     * @param {String} ssid - Mac address (bssid) or name (essid) of the access point.
     * @param {Boolean|String} password - Password of the connection or false for no password.
     * @param {Boolean} hidden - set to true when connecting for the first time to an AP not broadcasting its SSID. Otherwise the SSID would not be found and the connection attempt would fail.
     * @param {Boolean} private - if set to true, the connection will only be visible to the user who created it. Otherwise the connection is system-wide, which is the default.
     * @param {Function} callback - Boolean
     */
    Connect: function (interfaceName, connectionName, ssid, password, hidden, private, callback) {
        require("../devices").Exists(interfaceName, (exists) => {
            if (exists === true) {
                // @ts-ignore
                hidden = hidden === true ? "yes" : "no";
                // @ts-ignore
                private = private === true ? "yes" : "no";
                var command = "nmcli device wifi connect " + ssid + " ifname " + interfaceName + " hidden " + hidden + " private " + private;
                if (password !== false) {
                    command += " password '" + password + "'";
                }
                if (connectionName !== false) {
                    command += " name '" + connectionName + "'";
                }
                basis.System.ShellCommand(command, (e) => {
                    callback(e.stderr === "");
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * Create a Wi-Fi hotspot.
     * @param {String} interfaceName
     * @param {Boolean|String} connectionName - Name of the connection or false for nmcli auto naming.
     * @param {String} ssid - SSID of the hotspot.
     * @param {String} password - Password to use for the created hotspot.
     * @param {Boolean|Number} channel - Wi-Fi channel to use or false for auto.
     * @param {Boolean|Number} band - Wi-Fi band to use or false for auto.
     * @param {Function} callback - Boolean
     */
    Hotspot: function (interfaceName, connectionName, ssid, password, channel, band, callback) {
        require('../devices').Exists(interfaceName, (exists) => {
            if (exists === true) {
                var command = "nmcli device wifi hotspot ifname " + interfaceName + " con-name '" + connectionName + "' ssid '" + ssid + "' password '" + password + "'";
                if (channel !== false) {
                    command += " channel " + channel;
                }
                if (band !== false) {
                    command += " band " + band;
                }
                basis.System.ShellCommand(command, (e) => {
                    callback(e.stderr === "");
                });
            } else {
                callback(false);
            }
        });
    }
};