var basis = require('alpi_basis');

module.exports = {
    IsNmcliInstalled: function (callback) {
        basis.System.PackageManager((packageManager) => {
            if (packageManager === "pacman") {
                basis.System.PackageIsInstalled("networkmanager", (installed) => {
                    callback(installed);
                });
            } else if (packageManager === "apt") {
                basis.System.PackageIsInstalled("network-manager", (installed) => {
                    callback(installed);
                });
            } else {
                //it is assumed it is installed
                callback(true);
            }
        });
    },
    General: require('./general'),
    Devices: require('./devices'),
    Networking: require('./networking'),
    Radio: require('./radio'),
    Connection: require('./connection')
};