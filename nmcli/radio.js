var basis = require('alpi_basis');

module.exports = {
    /**
     * Get status of all radio switches.
     * @param {Function} callback - false/Object that contains all radio switches status.
     */
    GetRadioStatus: function (callback) {
        basis.System.ShellCommand("nmcli -t radio", (informations) => {
            if (informations.stderr === "") {
                informations = informations.stdout.split(":");
                callback({
                    "wifi-hw": informations[0],
                    "wifi": informations[1],
                    "wwan-hw": informations[2],
                    "wwan": informations[3]
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * Set status of all radio switches.
     * @param {Boolean} status - Status to set.
     * @param {Function} callback - Boolean.
     */
    SetRadioStatus: function (status, callback) {
        // @ts-ignore
        status = status === true ? "on" : "off";
        basis.System.ShellCommand("nmcli radio " + status, (e) => {
            callback(e.stderr === "");
        });
    },
    /**
     * Get status of Wi-Fi radio switch.
     * @param {Function} callback - false/String that contains wifi radio switch status.
     */
    GetWifiStatus: function (callback) {
        basis.System.ShellCommand("nmcli radio wifi", (status) => {
            callback(status.stderr === "" ? status.stdout : false);
        });
    },
    /**
     * Set status of Wi-Fi radio switches.
     * @param {Boolean} status - Status to set.
     * @param {Function} callback - Boolean.
     */
    SetWifiStatus: function (status, callback) {
        // @ts-ignore
        status = status === true ? "on" : "off";
        basis.System.ShellCommand("nmcli radio wifi " + status, (e) => {
            callback(e.stderr === "");
        });
    },
    /**
     * Get status of mobile broadband radio switch.
     * @param {Function} callback - false/String that contains mobile boradband radio switch status.
     */
    GetMobileBroadbandStatus: function (callback) {
        basis.System.ShellCommand("nmcli radio wwan", (status) => {
            callback(status.stderr === "" ? status.stdout : false);
        });
    },
    /**
     * Set status of mobile broadband radio switches.
     * @param {Boolean} status - Status to set.
     * @param {Function} callback - Boolean.
     */
    SetMobileBroadbandStatus: function (status, callback) {
        // @ts-ignore
        status = status === true ? "on" : "off";
        basis.System.ShellCommand("nmcli radio wwan " + status, (e) => {
            callback(e.stderr === "");
        });
    }
};