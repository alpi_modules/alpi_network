var basis = require('alpi_basis');
var filters = require('alpi_filters');

module.exports = {
    /**
     * @summary List built-in chains.
     * @param {Function} callback 
     */
    List: function (callback) {
        basis.System.ShellCommand("sudo iptables -L -v | grep 'Chain' | grep 'INPUT\\|FORWARD\\|OUTPUT'", (e) => {
            if (e.stderr === "") {
                var informations = e.stdout.trim().split("\n");
                var chains = {};
                informations.forEach(line => {
                    line = line.split(" ");
                    chains[line[1]] = {
                        name: line[1],
                        policy: line[3],
                        packets: line[4],
                        bytes: line[6]
                    };
                });
                callback(chains);
            } else {
                callback(e);
            }
        });
    },
    /**
     * @summary Check if the parameter match one of the built-in chains.
     * @param {String} chainName 
     */
    Exists: function (chainName) {
        return chainName == "INPUT" || chainName == "FORWARD" || chainName == "OUTPUT";
    },
    /**
     * @summary Flush the selected chain.
     * @description Remove all rules of the selected chain.
     * @param {String} chainName One of the three built-in chains: INPUT, OUTPUT, FORWARD.
     * @param {Function} callback 
     */
    Flush: function (chainName, callback) {
        if (module.exports.Exists(chainName) === true) {
            basis.System.ShellCommand("sudo iptables -F " + chainName, (e) => {
                callback(e.stderr === "");
            });
        } else {
            callback(false);
        }
    },
    /**
     * @summary Flush all the built-in chains.
     * @description Remove all rules from all the built-in chains.
     * @param {Function} callback 
     */
    FlushAll: function (callback) {
        basis.System.ShellCommand("sudo iptables -F INPUT && sudo iptables -F FORWARD && sudo iptables -F OUTPUT", (e) => {
            callback(e.stderr === "");
        });
    },
    /**
     * @summary Change the default policy ot the selected chain.
     * @param {String} chainName One of the three built-in chains: INPUT, OUTPUT, FORWARD.
     * @param {String} policy The possible policies are: ACCEPT and DROP.
     * @param {Function} callback 
     */
    ChangeDefaultPolicy: function (chainName, policy, callback) {
        if (module.exports.Exists(chainName) === true) {
            if (policy === "DROP" || policy == "ACCEPT") {
                basis.System.ShellCommand("sudo iptables -P " + chainName + " " + policy, (e) => {
                    callback(e.stderr === "");
                });
            } else {
                callback(false);
            }
        } else {
            callback(false);
        }
    },
    /**
     * @summary Manage customs chains.
     */
    Customs: require('./customs'),
    /**
     * @summary List rules of a selected chain with their informatiosn.
     * @param {String} chainName One of the three built-in chains: INPUT, OUTPUT, FORWARD.
     * @param {Function} callback 
     */
    ListRules: function (chainName, callback) {
        if (module.exports.Exists(chainName) === true) {
            basis.System.ShellCommand("sudo iptables -v -n -L " + chainName + " | grep -v 'Chain\\|pkts' | tr -s ' '", (e) => {
                if (e.stderr === "") {
                    var informations = e.stdout.trim().split('\n');
                    var rules = {};
                    informations.forEach(line => {
                        line = line.split(' ');
                        rules[line[0]] = {
                            number: line[0],
                            packets: line[1],
                            bytes: line[2],
                            target: line[3],
                            protocol: line[4],
                            opt: line[5],
                            interface: line[6],
                            out: line[7],
                            source: line[8],
                            destination: line[9]
                        };
                    });
                    if (Object.keys(rules).length == 1 && rules.hasOwnProperty('')) {
                        callback(undefined);
                    } else {
                        callback(rules);
                    }
                } else {
                    callback(false);
                }
            });
        } else {
            callback(false);
        }
    },
    /**
     * @summary List rules of a selected chain in a command line form.
     * @param {String} chainName One of the three built-in chains: INPUT, OUTPUT, FORWARD.
     * @param {Function} callback 
     */
    ListRulesCommands: function (chainName, callback) {
        chainName = filters.KeepOnly.LettersAndHyphens(chainName);
        if (module.exports.Exists(chainName) === true) {
            basis.System.ShellCommand("sudo iptables -S " + chainName, (e) => {
                if (e.stderr === "") {
                    callback(e.stdout.trim().split('\n'));
                } else {
                    callback(false);
                }
            });
        } else {
            callback(false);
        }
    },
    /**
     * @summary Create a new rule.
     * @param {String} chainName One of the three built-in chains: INPUT, OUTPUT, FORWARD.
     * @param {String} target One of the possible targets, a list of targets is accessible from Iptables.Targets .
     * @param {String} protocol One of the possible protocols, a list of protocols is accessible from alpi_network.Protocols .
     * @param {String} inInterface The name of a network interface, a list of network interfaces is accessible from alpi_network.Nmcli.Devices .
     * @param {String} outInterface The name of a network interface.
     * @param {String} source An ip address IPV4/IPV6.
     * @param {String} destination An ip address IPV4/IPV6.
     * @param {Function} callback 
     */
    CreateRule: function (chainName, target, protocol, inInterface, outInterface, source, destination, callback) {
        if (module.exports.Exists(chainName)) {
            if (require('../targets').Exists(target)) {
                require('../../protocols').Exists(protocol, (exists) => {
                    if (exists === true) {
                        require('../../nmcli/devices').Exists(inInterface, (exists) => {
                            if (exists === true) {
                                require('../../nmcli/devices').Exists(outInterface, (exists) => {
                                    if (exists === true) {
                                        if (filters.Validate.IpV4(source) || filters.Validate.IpV6(source)) {
                                            if (filters.Validate.IpV4(destination) || filters.Validate.IpV6(destination)) {
                                                basis.System.ShellCommand("sudo iptables -A " + chainName + " -j " + target + " -p " + protocol + " -i " + inInterface + " -o " + outInterface + " -s " + source + " -d " + destination, (e) => {
                                                    callback(e.stderr === "");
                                                });
                                            } else {
                                                callback(false);
                                            }
                                        } else {
                                            callback(false);
                                        }
                                    } else {
                                        callback(false);
                                    }
                                });
                            } else {
                                callback(false);
                            }
                        });
                    } else {
                        callback(false);
                    }
                })
            } else {
                callback(false);
            }
        } else {
            callback(false);
        }
    },
    /**
     * @summary Delete a rule from a specific chain and a rule number
     * @param {String} chainName One of the three built-in chains: INPUT, OUTPUT, FORWARD.
     * @param {String} ruleNumber The number of a rule, you can list the rules of a chain using the function ListRules.
     * @param {*} callback 
     */
    DeleteRule: function (chainName, ruleNumber, callback) {
        if (module.exports.Exists(chainName) === true) {
            module.exports.ListRules(chainName, (rules) => {
                if (Object.keys(rules).includes(ruleNumber)) {
                    basis.System.ShellCommand("sudo iptables -D " + chainName + " " + ruleNumber, (e) => {
                        callback(e.stderr === "");
                    });
                } else {
                    callback(false);
                }
            });
        } else {
            callback(false);
        }
    }
};