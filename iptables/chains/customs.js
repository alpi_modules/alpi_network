var basis = require('alpi_basis');
var filters = require('alpi_filters');

module.exports = {
    /**
     * @summary List custom chains.
     * @param {Function} callback 
     */
    List: function (callback) {
        basis.System.ShellCommand("sudo iptables -L -v | grep Chain | grep -v 'INPUT\\|FORWARD\\|OUTPUT' | tr -d '('", (e) => {
            if (e.stderr === "") {
                var informations = e.stdout.trim().split('\n');
                var customChains = {};
                informations.forEach(line => {
                    line = line.split(" ");
                    customChains[line[1]] = {
                        name: line[1],
                        reference: line[2]
                    };
                });
                callback(customChains);
            } else {
                callback(false);
            }
        });
    },
    /**
     * @summary Check if the parameter match one of the custom chains.
     * @param {String} chainName One of the custom chains, you can list the custom chains using the function List.
     * @param {Function} callback 
     */
    Exists: function (chainName, callback) {
        chainName = filters.KeepOnly.LettersAndHyphens(chainName);
        module.exports.List(chains => {
            var exists = false;
            Object.keys(chains).forEach(chain => {
                if (chainName == chain) {
                    exists = true;
                }
            });
            callback(exists);
        });
    },
    /**
     * @summary Create a new custom chain.
     * @param {String} chainName The chain name must only contains letters and hyphens, all others caracters will be automatically removed.
     * @param {Function} callback 
     */
    Create: function (chainName, callback) {
        chainName = filters.KeepOnly.LettersAndHyphens(chainName);
        module.exports.Exists(chainName, (exists) => {
            if (exists === false) {
                basis.System.ShellCommand("sudo iptables -N " + chainName, (e) => {
                    callback(e.stderr === "");
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * @summary Delete the selected built-in chain.
     * @param {String} chainName One of the custom chains, you can list the custom chains using the function List.
     * @param {Function} callback 
     */
    Delete: function (chainName, callback) {
        chainName = filters.KeepOnly.LettersAndHyphens(chainName);
        module.exports.Exists(chainName, (exists) => {
            if (exists === true) {
                basis.System.ShellCommand("sudo iptables -X " + chainName, (e) => {
                    callback(e.stderr === "");
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * @summary Delete all of the custom chains.
     * @param {Function} callback 
     */
    DeleteAll: function (callback) {
        basis.System.ShellCommand("sudo iptables -X", (e) => {
            callback(e.stderr === "");
        });
    },
    /**
     * @summary Flush the selected chain.
     * @description Remove all rules of the selected chain.
     * @param {String} chainName One of the custom chains, you can list the custom chains using the function List.
     * @param {Function} callback 
     */
    Flush: function (chainName, callback) {
        chainName = filters.KeepOnly.LettersAndHyphens(chainName);
        module.exports.Exists(chainName, (exists) => {
            if (exists === true) {
                basis.System.ShellCommand("sudo iptables -F " + chainName, (e) => {
                    callback(e.stderr === "");
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * @summary Flush all the custom chains.
     * @description Remove all rules from all the custom chains.
     * @param {Function} callback 
     */
    FlushAll: function (callback) {
        var flushedChains = {};
        module.exports.List(customChains => {
            if (customChains != false) {
                Object.keys(customChains).forEach(chain => {
                    module.exports.Flush(chain, (e) => {
                        flushedChains[chain] = e;
                    });
                });
                callback(flushedChains);
            } else {
                callback(false);
            }
        });
    },
    /**
     * @summary List rules of a selected chain with their informatiosn.
     * @param {String} chainName One of the custom chains, you can list the custom chains using the function List.
     * @param {Function} callback 
     */
    ListRules: function (chainName, callback) {
        chainName = filters.KeepOnly.LettersAndHyphens(chainName);
        module.exports.Exists(chainName, (exists) => {
            if (exists === true) {
                basis.System.ShellCommand("sudo iptables -v -n -L " + chainName + " | grep -v 'Chain\\|pkts' | tr -s ' '", (e) => {
                    if (e.stderr === "") {
                        var informations = e.stdout.trim().split('\n');
                        var rules = {};
                        informations.forEach(line => {
                            line = line.split(' ');
                            rules[line[0]] = {
                                number: line[0],
                                reference: line[1],
                                target: line[2],
                                protocol: line[3],
                                opt: line[4],
                                interface: line[5],
                                out: line[6],
                                source: line[7],
                                destination: line[8]
                            };
                        });
                        if (Object.keys(rules).length == 1 && rules.hasOwnProperty('')) {
                            callback(undefined);
                        } else {
                            callback(rules);
                        }
                    } else {
                        callback(false);
                    }
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * @summary List rules of a selected chain in a command line form.
     * @param {String} chainName One of the custom chains, you can list the custom chains using the function List.
     * @param {Function} callback 
     */
    ListRulesCommands: function (chainName, callback) {
        chainName = filters.KeepOnly.LettersAndHyphens(chainName);
        module.exports.Exists(chainName, (exists) => {
            if (exists === true) {
                basis.System.ShellCommand("sudo iptables -S " + chainName, (e) => {
                    if (e.stderr === "") {
                        callback(e.stdout.trim().split('\n'));
                    } else {
                        callback(false);
                    }
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * @summary Create a new rule.
     * @param {String} chainName One of the custom chains, you can list the custom chains using the function List.
     * @param {String} target One of the possible targets, a list of targets is accessible from Iptables.Targets .
     * @param {String} protocol One of the possible protocols, a list of protocols is accessible from alpi_network.Protocols .
     * @param {String} inInterface The name of a network interface, a list of network interfaces is accessible from alpi_network.Nmcli.Devices .
     * @param {String} outInterface The name of a network interface.
     * @param {String} source An ip address IPV4/IPV6.
     * @param {String} destination An ip address IPV4/IPV6.
     * @param {Function} callback 
     */
    CreateRule: function (chainName, target, protocol, inInterface, outInterface, source, destination, callback) {
        chainName = filters.KeepOnly.LettersAndHyphens(chainName);
        module.exports.Exists(chainName, (exists) => {
            if (exists === true) {
                if (require('../targets').Exists(target)) {
                    require('../../protocols').Exists(protocol, (exists) => {
                        if (exists === true) {
                            require('../../nmcli/devices').Exists(inInterface, (exists) => {
                                if (exists === true) {
                                    require('../../nmcli/devices').Exists(outInterface, (exists) => {
                                        if (exists === true) {
                                            if (filters.Validate.IpV4(source) || filters.Validate.IpV6(source)) {
                                                if (filters.Validate.IpV4(destination) || filters.Validate.IpV6(destination)) {
                                                    basis.System.ShellCommand("sudo iptables -A " + chainName + " -j " + target + " -p " + protocol + " -i " + inInterface + " -o " + outInterface + " -s " + source + " -d " + destination, (e) => {
                                                        callback(e.stderr === "");
                                                    });
                                                } else {
                                                    callback(false);
                                                }
                                            } else {
                                                callback(false);
                                            }
                                        } else {
                                            callback(false);
                                        }
                                    });
                                } else {
                                    callback(false);
                                }
                            });
                        } else {
                            callback(false);
                        }
                    })
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * @summary Delete a rule from a specific chain and a rule number
     * @param {String} chainName One of the custom chains, you can list the custom chains using the function List.
     * @param {String} ruleNumber The number of a rule, you can list the rules of a chain using the function ListRules.
     * @param {*} callback 
     */
    DeleteRule: function (chainName, ruleNumber, callback) {
        chainName = filters.KeepOnly.LettersAndHyphens(chainName);
        module.exports.Exists(chainName, (exists) => {
            if (exists === true) {
                module.exports.ListRules(chainName, (rules) => {
                    if (Object.keys(rules).includes(ruleNumber)) {
                        basis.System.ShellCommand("sudo iptables -D " + chainName + " " + ruleNumber, (e) => {
                            callback(e.stderr === "");
                        });
                    } else {
                        callback(false);
                    }
                });
            } else {
                callback(false);
            }
        });
    }
};