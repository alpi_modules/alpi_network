var basis = require('alpi_basis');

module.exports = {
    /**
     * @summary Check if the package iptables is installed.
     * @param {Function} callback 
     */
    CheckSetup: function (callback) {
        basis.System.PackageIsInstalled("iptables", (exists) => {
            if (exists === true) {
                callback(true);
            } else {
                callback(false);
            }
        });
    },
    /**
     * @summary List all chains with their rules.
     * @param {Function} callback 
     */
    ListAllChainsWithRules: function (callback) {
        var chains = {};
        new Promise((resolve, reject) => {
            module.exports.Chains.List(builtInChains => {
                if (builtInChains !== false) {
                    chains['built-in'] = builtInChains;
                    var processedRules = 0;
                    var error = false;
                    Object.keys(builtInChains).forEach(chain => {

                        module.exports.Chains.ListRules(chain, rules => {
                            if (error === false) {
                                if (rules !== false) {
                                    chains['built-in'][chain].rules = rules;
                                } else {
                                    error = true;
                                }
                            }
                            processedRules++;
                            if (processedRules === Object.keys(builtInChains).length) {
                                resolve(!error);
                            }
                        });
                    });
                } else {
                    resolve(false);
                }
            });
        }).then((result) => {
            return new Promise((resolve, reject) => {
                if (result === true) {
                    module.exports.Chains.Customs.List(customChains => {
                        if (customChains !== false) {
                            chains.custom = customChains;
                            var processedRules = 0;
                            var error = false;
                            Object.keys(customChains).forEach(chain => {
                                module.exports.Chains.Customs.ListRules(chain, rules => {
                                    if (error === false) {
                                        if (rules !== false) {
                                            chains.custom[chain].rules = rules;
                                        } else {
                                            error = true;
                                        }
                                    }
                                    processedRules++;
                                    if (processedRules === Object.keys(customChains).length) {
                                        resolve(!error);
                                    }
                                });
                            });
                        } else {
                            resolve(false);
                        }
                    });
                } else {
                    resolve(false);
                }
            });
        }).then((result) => {
            if (result === true) {
                callback(chains);
            } else {
                callback(false);
            }
        });
    },
    /**
     * @summary Manage chains;
     */
    Chains: require('./chains/chains'),
    /**
     * @summary Help with targets
     */
    Targets: require('./targets')
};