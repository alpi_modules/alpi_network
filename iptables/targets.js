module.exports = {
    /**
     * @summary List all possible targets.
     * @description More informations at: https://gist.github.com/mcastelino/c38e71eb0809d1427a6650d843c42ac2#targets-1 .
     * @return {Array}
     */
    List: function(){
        return ["ACCEPT", "DROP", "LOG", "DNAT", "RETURN", "SNAT", "REJECT", "ULOG", "MARK", "MASQUERADE", "REDIRECT"];
    },
    /**
     * @summary Check if the parameter match one of the posssible targets.
     * @param {String} targetName 
     */
    Exists: function(targetName){
        return module.exports.List().includes(targetName);
    }
};