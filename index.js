var basis = require('alpi_basis');

module.exports = {
    Nmcli: require("./nmcli/nmcli"),
    Iptables: require("./iptables/iptables"),
    Protocols: require('./protocols')
};